<?php

namespace Tests\Unit;

use App\Post;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;

    public function testApiEmptyList()
    {
        $response = $this->json('GET', '/api/posts');

        $response
            ->assertStatus(200)
            ->assertJson([
                'data' => [],
            ]);
    }

    public function testApiNotEmptyList()
    {
        factory(Post::class, 5)->create();
        $response = $this->json('GET', '/api/posts');

        $response
            ->assertStatus(200);

        $posts = json_decode($response->getContent(), true);

        $this->assertEquals(5, count($posts['data']));
    }

    /**
     * Test correct slug generation
     */
    public function testSlug()
    {
        $post = factory(Post::class)->create(['title' => 'The Amazing Post Title']);
        $this->assertEquals($post->slug, 'the-amazing-post-title');
    }

    public function testApiFailCreatePostInvalidTitle()
    {
        $user = factory(User::class)->states('UserA')->make();
        $post = [
            'title' => '',
            'text' => $this->faker()->realText(50),
            'author_id' => $user['id'],
        ];

        $response = $this->json('POST', '/api/posts', $post);

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'title' => [
                        'The title field is required.',
                    ],
                ],
            ]);
    }

    public function testApiFailCreatePostInvalidText()
    {
        $user = factory(User::class)->states('UserA')->make();
        $post = [
            'title' => $this->faker()->realText(10),
            'text' => '',
            'author_id' => $user['id'],
        ];

        $response = $this->json('POST', '/api/posts', $post);

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'text' => [
                        'The text field is required.',
                    ],
                ],
            ]);
    }

    public function testApiFailCreatePostInvalidUser()
    {
        $post = [
            'title' => $this->faker()->realText(10),
            'text' => $this->faker()->realText(50),
            'author_id' => 9999,
        ];

        $response = $this->json('POST', '/api/posts', $post);

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'author_id' => [
                        'The selected author id is invalid.',
                    ],
                ],
            ]);
    }

    public function testApiSuccessCreatePost()
    {
        $user = factory(User::class)->create();
        $post = [
            'title' => $this->faker()->realText(10),
            'text' => $this->faker()->realText(50),
            'author_id' => $user['id'],
        ];

        $response = $this->json('POST', '/api/posts', $post);

        $response
            ->assertStatus(201)
            ->assertJson([
                'data' => [
                    'title' => $post['title'],
                    'text' => $post['text'],
                ],
            ]);
    }

    public function testApiFailDeletePostNotFound()
    {
        $response = $this->json('DELETE', '/api/posts/wrong-id');

        $response
            ->assertStatus(404)
            ->assertJson([
                'message' => 'Post with id wrong-id not found',
            ]);

    }

    public function testApiSuccessDeletePost()
    {
        $post = factory(Post::class)->create();
        $response = $this->json('DELETE', '/api/posts/' . $post['id']);

        $response
            ->assertStatus(200);

    }
}

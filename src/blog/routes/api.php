<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * POSTS
 */
Route::prefix('posts')->namespace('Api')->group(function() {
    // Returns a list of posts
    Route::get('/', 'PostsController@list');

    // Store a new Post
    Route::post('/', 'PostsController@store');

    // Update a given post
    Route::put('/{id}', 'PostsController@update');

    // Delete a given post
    Route::delete('/{id}', 'PostsController@destroy');
});

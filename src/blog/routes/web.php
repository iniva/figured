<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Shows a list of Posts
Route::get('/', 'PostsController@index')
    ->name('home');

/**
 * POSTS
 */
Route::prefix('posts')->group(function() {
    // Redirect to Posts list
    Route::get('/', function () {
        return redirect()->route('home');
    });

    /**
     * Authenticated routes
     */
    Route::middleware('auth')->group(function () {
        // Show page to create a Post
        Route::get('/create', 'PostsController@create')
            ->name('post-create');

        // Show page to edit a given Post
        Route::get('/{slug}/edit', 'PostsController@edit')
            ->name('post-edit');
    });

    // Shows details of a given Post
    Route::get('/{slug}', 'PostsController@show')
        ->name('post-detail');
});

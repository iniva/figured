<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->_id,
            'author' => $this->author,
            'title' => $this->title,
            'slug' => "{$this->slug}-{$this->_id}",
            'link' => route('post-detail', ['slug' => "{$this->slug}-{$this->_id}"]),
            'text' => $this->text,
            'excerpt' => str_limit($this->text, 20),
            'created' => $this->created_at->toDayDateTimeString(),
            'updated' => $this->updated_at->toDayDateTimeString(),
        ];
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePost;
use App\Http\Requests\UpdatePost;
use App\Http\Resources\PostResource;
use App\Post;

class PostsController extends Controller
{
    public function list()
    {
        $posts = Post::orderBy('created_at', 'desc')->get();

        return PostResource::collection($posts);
    }

    public function store(StorePost $request)
    {
        $post = Post::create($request->validFields());

        return new PostResource($post);
    }

    public function update(UpdatePost $request, $id)
    {
        $post = Post::find($id);
        if ($post === null) {
            return response()->json([
                "message" => "Post with id {$id} not found",
            ])->setStatusCode(404);
        }

        $post->fill($request->validFields())->save();

        return new PostResource($post);
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        if ($post === null) {
            return response()->json([
                "message" => "Post with id {$id} not found",
            ])->setStatusCode(404);
        }

        $post->delete();

        return response()->json([
            "message" => "Post with id {$id} deleted",
        ]);
    }
}

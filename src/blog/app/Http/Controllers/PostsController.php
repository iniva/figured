<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePost;
use App\Http\Resources\PostResource;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    public function index()
    {
        return view('posts.index');
    }

    public function show(Request $request, $slug)
    {
        $slugParts = explode('-', $slug);
        $id = end($slugParts);
        $post = Post::find($id);
        $detail = (new PostResource($post))->toArray($request);

        return view('posts.detail', [
            'user' => json_encode(Auth::user()),
            'post' => json_encode($detail),
        ]);
    }

    public function create()
    {
        return view('posts.create', [
            'user' => Auth::user(),
        ]);
    }

    public function edit(Request $request, $slug)
    {
        $slugParts = explode('-', $slug);
        $id = end($slugParts);
        $post = Post::find($id);
        $detail = (new PostResource($post))->toArray($request);

        return view('posts.edit', [
            'user' => Auth::user(),
            'post' => json_encode($detail),
        ]);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApiRequest extends FormRequest
{
    public function validFields()
    {
        $allowedFields = array_keys($this->rules());
        $requestFields = collect($this->all());
        $validFields = $requestFields->filter(function ($value, $key) use ($allowedFields) {
            return in_array($key, $allowedFields);
        })->keys()->all();

        return $this->only($validFields);
    }
}

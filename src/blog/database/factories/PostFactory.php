<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(20),
        'text' => $faker->realText(150),
        'created_at' => now(),
        'updated_at' => now(),
        'author_id' => function () {
            return factory(User::class)->create()->id;
        },
    ];
});

$factory->state(\App\Post::class, 'FromUserA', function (Faker $faker) {
    return [
        'author_id' => 1
    ];
});

$factory->state(\App\Post::class, 'FromUserA', function (Faker $faker) {
    return [
        'author_id' => 2
    ];
});

<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->state(\App\User::class, 'UserA', function (Faker $faker) {
    return [
        'name' => 'User A',
        'email' => 'user_a@test.com',
        'id' => 1
    ];
});

$factory->state(\App\User::class, 'UserB', function (Faker $faker) {
    return [
        'name' => 'User B',
        'email' => 'user_b@test.com',
        'id' => 2
    ];
});

<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'figured',
            'email' => 'blog@figured.com',
            'password' => \Illuminate\Support\Facades\Hash::make('123456'),
        ]);
    }
}

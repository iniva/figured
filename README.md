# Figured Blog

## Environment preparation steps
If you are going to use the **docker-compose** implementation follow these steps

### Environment variables file
+ Rename `.env.example` file to `.env`

+ To avoid permission conflicts between the host machine and the php container you need to change the **_GID_** and 
**_UID_** values in the `.env` file.

To obtain your GID and UI
```bash
# In your terminal, type "id"
╰─$ id
# uid=1000(username) gid=1000(username)
```

### Update your hosts file [optional]
The nginx service is listening to every IP, but if you want you can add `blog.local` pointing to your machine. This is just for a good looking URL.
 
### Start up the local dev environment:
```bash
# Build local images
docker-compose build

# Build external images and Start local dev environment
docker-compose up -d
```

### Running commands in containers
There's a `run.sh` file at the root of this project to facilitate running commands inside the containers. It needs 
execution permissions.

```bash
# Installing backend related dependencies
./run.sh php "composer install"

# Installing frontend related dependencies
./run.sh php "npm install"

# Running migrations
./run.sh php "php artisan migrate"

# Seeding the database
# This will add a user with the following credentials
# email: blog@figured.com
# password: 123456
./run.sh php "php artisan db:seed"

# Running tests
./run.sh php "phpunit"
```

### Using the blog
Open your preferred browser (\**cof*\* chrome \**cof*\*) and go to [http://blog.local](http://blog.local) (or your local IP if you didn't do the optional step above)

If your phone is connected to the same network as your computer you can access the blog pointing your phone browser to your machine IP. If you are using Chrome on your phone you have the option "_Add to Home screen_". It's just a silly detail.

## Notes
Instead of going with the tipical blogging approach (wordpress, blogspot) of one admin per blog and all the other users are just guests, I went for a more Medium blogging approach: It's just one blog but with many bloggers. This means that any user can read the blog and any registered user has the ability to create a blog post but only the owner of a post can edit or delete it. 

The tech stack has a bit of everything:

+ **PHP**: Laravel 5.6 (on top of PHP FPM 7.2 + Nginx)
+ **Databases**: MongoDB (latest), MySQL 5.7
+ **Frontend**: VueJS 2+, Bootstrap 4
+ **Infrastructure**: Docker (using docker-compose along with Dockerfiles)

I don't usually combine these techs the way I did this time. My approach was purely experimental, I wanted to know if I could make it this way. Right here you have Laravel with VueJS embedded and a hybrid data layer using MySQL for Users and MongoDB for Posts. 

My usual go-to approach is more related to "separation of concerns". Ideally I will have backend and frontend in different projects. The backend would only act as an API so the frontends are free to implement any technology as they see fit. Also, because I think is a more simplistic approach, every project has just one responsibility to avoid unnecessary complexity. It's the same logic as docker containers: One container with one concern instead of one container with everything.
